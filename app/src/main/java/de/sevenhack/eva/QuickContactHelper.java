package de.sevenhack.eva;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

/**
 * Created by madhavajay on 20/03/2016.
 */

public final class QuickContactHelper {

    private static final String TAG = "QuickContactHelper";

    private static final String[] PHOTO_ID_PROJECTION = new String[] {
            ContactsContract.Contacts.PHOTO_ID
    };

    private static final String[] PHOTO_BITMAP_PROJECTION = new String[] {
            ContactsContract.CommonDataKinds.Photo.PHOTO
    };

    private final String phoneNumber;

    private final ContentResolver contentResolver;

    public QuickContactHelper(final Context context, final String phoneNumber) {
        Log.v(TAG, "phone number" + phoneNumber);
        this.phoneNumber = phoneNumber;
        contentResolver = context.getContentResolver();

    }

    public Bitmap getThumbnail() {
        Log.v(TAG, "add thumbnail");

        final Integer thumbnailId = fetchThumbnailId();
        if (thumbnailId != null) {
            final Bitmap thumbnail = fetchThumbnail(thumbnailId);
            if (thumbnail != null) {
                Log.v(TAG, "set image bitmap");
                return thumbnail;
            }
        }
        return null;
    }

    private Integer fetchThumbnailId() {
/*
ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME
        }, null, null, null);

 */


        final Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        final Cursor cursor = contentResolver.query(uri, new String[]{ContactsContract.PhoneLookup.PHOTO_ID}, null, null, null);
        Log.v(TAG, "fetchThumbnailId");
        try {
            Integer thumbnailId = null;
            if (cursor.moveToFirst()) {
                Log.v(TAG, "movetofirst");
                thumbnailId = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_ID));
            }
            Log.v(TAG, "thumbnail id " +thumbnailId);
            return thumbnailId;
        }
        finally {
            cursor.close();
        }

    }

    final Bitmap fetchThumbnail(final int thumbnailId) {

        final Uri uri = ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, thumbnailId);
        final Cursor cursor = contentResolver.query(uri, PHOTO_BITMAP_PROJECTION, null, null, null);

        try {
            Log.v(TAG, "fetch thumbnail");
            Bitmap thumbnail = null;
            if (cursor.moveToFirst()) {
                final byte[] thumbnailBytes = cursor.getBlob(0);
                if (thumbnailBytes != null) {
                    thumbnail = BitmapFactory.decodeByteArray(thumbnailBytes, 0, thumbnailBytes.length);
                }
            }
            Log.v(TAG, "return thumbnail");
            Log.v(TAG, "byte array " + thumbnail);
            return thumbnail;
        }
        finally {
            cursor.close();
        }

    }

}
