package de.sevenhack.eva;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by madhavajay on 19/03/2016.
 */
public class DataManager {

    private static final String HACK_KEY = "eva";
    private static final String CLIENT_ID = "clientId";
    private static final String POLL_ENABLED_KEY = "pollEnabled";
    private final static String ALARM_INTERVAL_KEY = "alarm_interval";
    private final static String ALARM_NEXT_KEY = "alarm_next";
    public static final String CALL_ANSWER = "call_answer";
    public static final String CALL_SILENCE = "call_silence";
    public static final String CALL_HANGUP = "call_hangup";
    private static final String SERVER_HOST = "serverHost";
    private static final String CLIENT_JSON = "clientJson";
    private static final String STATE_JSON = "stateJson";
    //public static final String SERVER_URL = "http://10.100.105.170:1337/";
    public static final String SERVER_URL = "http://metaverse.eu-gb.mybluemix.net/";


    public final static Integer ALARM_INTERVAL_DEFAULT = 1500;

    int break_start_start = 50;
    int break_start_end = 56;

    int break_end_start = 110;
    int break_end_end = 116;

    int football_start = 133;
    int football_end = 140;

    private static final String TAG = "DataManager";
    private Context context;

    private final Gson gson = new Gson();

    public DataManager(Context context) {
        this.context = context;
    }

    public boolean hasPair() {
        String isPaired = getClientId();
        if (!isPaired.equals("")) {
            return true;
        }
        return false;
    }

    public void unPairDevice() {
        setSettingKeyWithValue(CLIENT_ID, "");
    }

    public String getClientId() {
        return (String) getSetting(CLIENT_ID, "String", "");
    }

    public void setServerHost(String serverHost) {
        setSettingKeyWithValue(SERVER_HOST, serverHost);
    }

    public String getServerHost() {
        String serverHost = (String) getSetting(SERVER_HOST, "String", SERVER_URL);
        return serverHost;
    }

    public void setPollEnabled(boolean enabled) {
        int enabledInt = (enabled) ? 1 : 0;
        setSettingKeyWithValue(POLL_ENABLED_KEY, enabledInt);
    }

    public boolean getPollEnabled() {
        int enabledInt = (int) getSetting(POLL_ENABLED_KEY, "Integer", 1);
        return (enabledInt > 0) ? true : false;
    }

    // Do not call this function from the main thread. Otherwise,
    // an IllegalStateException will be thrown.
    public String getAdvertisingId() {
        AdvertisingIdClient.Info adInfo = null;
        try {
            adInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.context);
        } catch (IOException e) {
            // Unrecoverable error connecting to Google Play services (e.g.,
            // the old version of the service doesn't support getting AdvertisingId).
        } catch (GooglePlayServicesNotAvailableException e) {
            // Google Play services is not available entirely.
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        }
        final String id = adInfo.getId();
        if (id != null && !id.equals("")) {
            return id;
        }
        return null;
    }

    public void setStateJson(String stateJson) {
        setSettingKeyWithValue(STATE_JSON, stateJson);
    }

    public ArrayList<HashMap<String, Object>> getActions() {
        ArrayList<HashMap<String, Object>> actions = new ArrayList<HashMap<String, Object>>();
        Map<String, Object> state = getState();

        Boolean empty = (Boolean) state.get("empty");

        // timed events
        int time = getTime();

        if (time >= break_start_start - 1 && time <= break_start_end + 1) {
            /// break start
            HashMap<String, Object> action = new HashMap<String, Object>();
            action.put("start", break_start_start);
            action.put("end", break_start_end);
            action.put("type", "break_start");
            action.put("icon", "ic_alarm");
            action.put("icon_big", "ic_apple");
            action.put("title", "Break Starting");
            action.put("message", "Brought to you by Apple");
            actions.add(action);
        } else if (time >= break_end_start - 1 && time <= break_end_end + 1) {
            // break end
            HashMap<String, Object> action = new HashMap<String, Object>();
            action.put("start", break_end_start);
            action.put("end", break_end_end);
            action.put("type", "break_end");
            action.put("icon", "ic_alarm");
            action.put("icon_big", "ic_dominos");
            action.put("title", "Break Ending");
            action.put("message", "Brought to you by Dominos");
            actions.add(action);
        } else if (time >= football_start && time <= football_end) {
            // tickets
            HashMap<String, Object> action = new HashMap<String, Object>();
            action.put("start", football_start);
            action.put("end", football_end);
            action.put("type", "football");
            action.put("icon", "ic_ticket");
            action.put("icon_big", "ic_football");
            action.put("title", "FC Bayern Tickets");
            action.put("message", "Save 20% Buy Now!");
            action.put("url", "sport");
            actions.add(action);
        }


        if (empty == null || !empty) {

            List<LinkedTreeMap<String,Object>> eventQueue = (List<LinkedTreeMap<String,Object>>) state.get("eventQueue");

            if (eventQueue != null) {
                for(LinkedTreeMap<String, Object> event : eventQueue) {
                    String type = (String) event.get("type");


                    if (type.equals("phone")) {
                        Log.v(TAG, "got a PHONE related instruction");
                        LinkedTreeMap<String,Object> data = null;
                        String callAction = null;
                        try {
                            Log.v(TAG, "getting data payload");
                            data = (LinkedTreeMap<String,Object>) event.get("data");
                            callAction = (String) data.get("action");
                            Log.v(TAG, "got this callAction from data" + callAction);

                            if (callAction != null) {
                                if (callAction.equals("answer")) {
                                    this.postNotification(CALL_ANSWER);
                                } else if (callAction.equals("silence")) {
                                    this.postNotification(CALL_HANGUP);
                                }
                            }
                        } catch (Exception e) {
                            Log.v(TAG, "error data payload failed");
                        }
                    } else {
                        LinkedTreeMap<String,Object> data = null;
                        String url = null;
                        try {
                            Log.v(TAG, "getting data payload");
                            data = (LinkedTreeMap<String,Object>) event.get("data");
                            url = (String) data.get("url");
                            Log.v(TAG, "got this url from data" + url);
                        } catch (Exception e) {
                            Log.v(TAG, "error data payload failed");
                        }

                        HashMap<String, Object> action = new HashMap<String, Object>();
                        action.put("type", type);
                        if (url != null) {
                            Log.v(TAG, "type " + type + " url " + url);
                            action.put("url", url);
                        }

                        actions.add(action);
                    }

                }
            }
        }

        return actions;
    }

    public void clearActionType(String type) {
        setSettingKeyWithValue(type, 0);
    }

    public void blockActionType(String type) {
        setSettingKeyWithValue(type, 1);
    }

    public void clearOlderActions(Integer time) {

        if (time < break_start_start - 10 || time > break_start_end + 10) {
            clearActionType("break_start");
        }

        if (time < break_end_start - 10 || time > break_end_end + 10) {
            clearActionType("break_end");
        }

        if (time < football_start - 10 || time > football_end + 10) {
            clearActionType("football");
        }
    }

    public boolean isActionTypeRunning(String type) {
        Integer running = (Integer) getSetting(type, "Integer", 0);
        if (running > 0) {
            return true;
        }

        return false;
    }

    public Integer getTime() {
        Map<String, Object> state = getState();
        Double timeDouble = (Double) state.get("serverTime");
        Integer time = timeDouble.intValue();
        return time;
    }

    public Map<String, Object> getState() {
        String stateJson = (String) getSetting(STATE_JSON, "String", "{\"empty\":true}");
        GsonBuilder builder = new GsonBuilder();
        Map o = (Map) builder.create().fromJson(stateJson, Object.class);
        return o;
    }

    public void setClientJson(String clientJson) {
        Log.v(TAG, "client Json " + clientJson );
        GsonBuilder builder = new GsonBuilder();
        Map o = (Map) builder.create().fromJson(clientJson, Object.class);

        Map data = (Map) o.get("data");
        String clientId = (String) data.get("clientId");

        setSettingKeyWithValue(CLIENT_JSON, clientJson);
        setSettingKeyWithValue(CLIENT_ID, clientId);
    }

    private SharedPreferences getSharedPreferences() {
        SharedPreferences settings = this.context.getSharedPreferences(HACK_KEY, Context.MODE_PRIVATE);
        return settings;
    }

    private Object getSetting(String settingKey, String type, Object defaultVal) {
        SharedPreferences settings = this.getSharedPreferences();
        if (type.equals("String")) {
            return settings.getString(settingKey, defaultVal.toString());
        } else if (type.equals("Integer")) {
            int settingsInt = settings.getInt(settingKey, (int)defaultVal);
            return new Integer(settingsInt);
        }

        return null;
    }

    public int getAlarmInterval() {
        Integer alarmInterval = (Integer)this.getSetting(ALARM_INTERVAL_KEY, "Integer", 0);
        if (alarmInterval == 0) {
            return ALARM_INTERVAL_DEFAULT;
        }
        return alarmInterval;
    }

    public int getNextAlarm() {
        Integer nextAlarm = (Integer)this.getSetting(ALARM_NEXT_KEY, "Integer", -1);
        return nextAlarm;
    }

    private void setSettingKeyWithValue(String key, Integer value) {
        SharedPreferences settings = this.getSharedPreferences();
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value.intValue());
        editor.commit();
    }

    private void setSettingKeyWithValue(String key, String value) {
        SharedPreferences settings = this.getSharedPreferences();
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private void postNotification(String intentName) {
        Intent intent = new Intent(intentName);
        LocalBroadcastManager.getInstance(this.context).sendBroadcast(intent);
    }
}
