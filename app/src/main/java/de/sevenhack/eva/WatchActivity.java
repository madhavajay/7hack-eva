package de.sevenhack.eva;

import android.app.ActionBar;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;

/**
 * Created by madhavajay on 19/03/2016.
 */
public class WatchActivity extends Activity {
    VideoView mVideoView;

    private static final String TAG = "WatchActivity";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getActionBar();
        actionBar.hide();

        setContentView(R.layout.activity_watch);

        if (!io.vov.vitamio.LibsChecker.checkVitamioLibs(this)) {
            finish();
        } else {
        }

        //String movieUrl = "http://10.100.105.175:1935/hubba/hurdles/playlist.m3u8;"; // doesnt work
        //String movieUrl = "rtsp://10.100.105.175:1935/hubba/hurdles"; // works
        String movieUrl = "rtmp://10.100.105.175:1935/hubba/mp4:hurdles"; // works best

        mVideoView = (VideoView) findViewById(R.id.video_view);

        mVideoView.setVideoPath(movieUrl);
        mVideoView.setMediaController(new MediaController(this));
        mVideoView.requestFocus();

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                // optional need Vitamio 4.0
                mediaPlayer.setPlaybackSpeed(1.0f);
            }
        });

        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();

            }
        });

    }

    @Override
    protected void onDestroy() {
        if (mVideoView != null) {
            mVideoView.stopPlayback();
            mVideoView = null;
        }
        super.onDestroy();
    }
}