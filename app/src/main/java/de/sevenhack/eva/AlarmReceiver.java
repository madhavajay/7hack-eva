package de.sevenhack.eva;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;

/**
 * Created by madhavajay on 19/03/2016.
 */

public class AlarmReceiver extends BroadcastReceiver {

    private Context context;
    private static final String TAG = "AlarmManager";
    private static final String EXTRAS_ALARM_INTERVAL_KEY = "alarm_interval_key";
    private static final String EXTRAS_ALARM_SHOW_KEY = "alarm_show_key";
    private static final String EXTRAS_ALARM_TEST_KEY = "alarm_test_key";
    private static final String HACK_KEY = "7hack";
    private final static String STANDUP_ALARM_NEXT_KEY = "alarm_next";
    private static long lastTimePoll = 0;
    private static long lastLiveEventTime = 0;

    private Context localContext;

    private DataManager dataManager;

    public static void createAlarm(Context context, int alarmInterval) {
        Log.v(TAG, "creating new alarm");
        Long time = new GregorianCalendar().getTimeInMillis() + alarmInterval;

        // store time for the next alarm
        SharedPreferences settings = context.getSharedPreferences(HACK_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(STANDUP_ALARM_NEXT_KEY, time.intValue());
        editor.commit();

        Intent intentAlarm = new Intent(context, AlarmReceiver.class);
        intentAlarm.putExtra(EXTRAS_ALARM_INTERVAL_KEY, alarmInterval);
        intentAlarm.putExtra(EXTRAS_ALARM_SHOW_KEY, 1);

        // create the object
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        //set the alarm for particular time
        alarmManager.set(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(context, 1, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            localContext = context;
            // gate to 1 second
            long now = System.currentTimeMillis();
            long diff = now - lastTimePoll;
            if (diff < 300) {
                Log.v(TAG, "gating the broadcast receiver");
                //return;
            }
            lastTimePoll = now;
            if (intent.getAction().equals(NetworkManager.POLL_RESPONSE)) {
                ArrayList<HashMap<String, Object>> actions = dataManager.getActions();
                runActions(actions);
            }
        }
    };

    private void runActions(ArrayList<HashMap<String, Object>> actions) {
        Integer time = dataManager.getTime();
        dataManager.clearOlderActions(time);
        Log.v(TAG, "Run Actions Loop: " + time);
        if (actions != null && time != null) {
            for(HashMap<String, Object> action : actions) {
                String type = (String) action.get("type");

                if (type.equals("football") || type.equals("break_start") || type.equals("break_end")) {
                    Log.v(TAG, "timed events");
                    Integer start = (Integer) action.get("start");
                    Integer end = (Integer) action.get("end");
                    boolean isRunning = dataManager.isActionTypeRunning(type);
                    //Log.v(TAG, "action type " + type + " start: " + start + " end " + end + " time: " + time);
                    if (time >= start && time <= end && !isRunning) {
                        Log.v(TAG, "time: " + time + " action: " + type + " GO");
                        sendNotification(action);
                        dataManager.blockActionType(type);
                    }
                } else if (type.equals("tickets")) {
                    long now = System.currentTimeMillis();
                    long diff = now - lastLiveEventTime;
                    Log.v(TAG, "liev event time gate " + now + " " + lastLiveEventTime + " " + diff);
                    if (diff > 10000) {
                        Log.v(TAG, "dont re leaunch the tickets");
                        lastLiveEventTime = now;
                        Log.v(TAG, "launch ticket page");
                        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                        PowerManager.WakeLock wl = powerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.FULL_WAKE_LOCK, "");
                        wl.acquire();

                        Intent i = new Intent(this.context, WebViewActivity.class);
                        //This flag is required for starting an activity outside of an activity.
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        String url = (String) action.get("url");

                        Bundle extras = new Bundle();
                        extras.putString("url", url);

                        i.putExtras(extras);

                        localContext.startActivity(i);
                        wl.release();
                    }
                } else if (type.equals("answer") || type.equals("hangup")) {
                    Log.v(TAG, "real time events");
                    Log.v(TAG, "process event " + type);
                }
            }
        }
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        this.context = context;
        dataManager = new DataManager(context);

        // Register LocalBroadcastNotifcations
        LocalBroadcastManager.getInstance(context).registerReceiver(this.messageReceiver, new IntentFilter(NetworkManager.POLL_RESPONSE));

        NetworkManager.getInstance().pollServer(context);

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    boolean pollEnabled = dataManager.getPollEnabled();
                    if (!pollEnabled) {
                        return;
                    }

                    int alarmInterval = intent.getIntExtra(EXTRAS_ALARM_INTERVAL_KEY, 0);
                    int showAlert = intent.getIntExtra(EXTRAS_ALARM_SHOW_KEY, 0);
                    int alarmTest = intent.getIntExtra(EXTRAS_ALARM_TEST_KEY, 0);
                    if (alarmInterval == 0) {
                        // get the one from system perferences
                        alarmInterval = dataManager.ALARM_INTERVAL_DEFAULT;
                    }

                    if (alarmTest == 0) {
                        AlarmReceiver.createAlarm(context, alarmInterval);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private int getIconId(String iconName) {
        String packageName = context.getPackageName();
        final ApplicationInfo applicationInfo;
        int resId = context.getResources().getIdentifier(iconName, "mipmap", packageName);
        return resId;
    }

    private void sendNotification(HashMap<String, Object> details) {
        Log.v(TAG, "sending notification where are my logs?");
        int notificationId = 1;

        int iconNotification = getIconId((String) details.get("icon"));
        int iconWearable = getIconId((String) details.get("icon_big"));

        String notificationTitle = (String) details.get("title");
        String message = (String) details.get("message");
        String type = (String) details.get("type");

        // Build intent for notification content
        Intent viewIntent = null;
        String url = null;
        try {
            url = (String) details.get("url");
        } catch(Exception e) {

        }
        if (url != null) {
            //viewIntent = new Intent(Intent.ACTION_VIEW);
            //viewIntent.setData(Uri.parse((String) details.get("url")));

            viewIntent = new Intent(this.context, WebViewActivity.class);

            Bundle extras = new Bundle();
            extras.putString("url", url);
            extras.putInt("notificationId", notificationId);
            viewIntent.putExtras(extras);

            Log.v(TAG, "url extra in alarm receiver " + extras.getString("url"));

        } else {
            viewIntent = new Intent(this.context, MainActivity.class);
        }

        PendingIntent viewPendingIntent = PendingIntent.getActivity(this.context, 0, viewIntent, 0);

        NotificationCompat.WearableExtender wearableExtender =
                new NotificationCompat.WearableExtender()
                        .setHintHideIcon(false)
                        .setBackground(BitmapFactory.decodeResource(
                                this.context.getResources(), iconWearable))
                        .clearActions();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this.context)
                // ..- .--. = UP in morse code
                // wait, on, wait, on, wait etc...
                .setVibrate(new long[]{0, 200, 200, 200, 200, 400, 200, 200, 200, 400, 200, 400, 200, 200})
                .setSmallIcon(iconNotification)
                .setColor(Color.RED)
                .setContentTitle(notificationTitle)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setLocalOnly(false)
                .setAutoCancel(true) //???
                .extend(wearableExtender);

        if (url != null) {
            notificationBuilder.setContentIntent(viewPendingIntent);
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this.context);

        notificationManager.notify(notificationId, notificationBuilder.build());

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(this.context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
