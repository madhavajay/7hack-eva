package de.sevenhack.eva;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by madhavajay on 20/03/2016.
 */

public class IncomingCallReceiver extends BroadcastReceiver
{
    private static final String TAG = "IncomingCallReceiver";

    Context mContext;

    @Override
    public void onReceive(final Context mContext, Intent intent)
    {
        Log.v(TAG, "incoming call broadcast received");
        try
        {

            Log.v(TAG, "forcing hidden from incoming call reciever");
            Intent i = new Intent(mContext, AcceptCallActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            mContext.startActivity(intent);
        }
        catch(Exception e)
        {
            //your custom message
        }

    }

}