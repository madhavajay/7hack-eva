package de.sevenhack.eva;

import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by madhavajay on 19/03/2016.
 */

public class PairActivity extends Activity {

    private static final String TAG = "PairActivity";

    private DataManager dataManager;

    private Button pairButton;
    private EditText pairInput;
    private EditText githubInput;
    private EditText serverHostInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getActionBar();
        actionBar.hide();

        dataManager = new DataManager(this);
        setContentView(R.layout.activity_pair);

        // Register LocalBroadcastNotifcations
        LocalBroadcastManager.getInstance(this).registerReceiver(this.messageReceiver, new IntentFilter(NetworkManager.PAIR_RESPONSE));

        pairButton = (Button) findViewById(R.id.button_login);
        pairInput = (EditText) findViewById(R.id.input_pair);
        githubInput = (EditText) findViewById(R.id.input_github);
        serverHostInput = (EditText) findViewById(R.id.input_server_host);
        serverHostInput.setText(dataManager.SERVER_URL);

        pairButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //pairButton.setEnabled(false);
                String pairCode = pairInput.getText().toString();
                String githubUsername = githubInput.getText().toString();
                String serverHost = serverHostInput.getText().toString();
                dataManager.setServerHost(serverHost);
                NetworkManager.getInstance().pairDevice(getApplication(), pairCode, githubUsername);
            }
        });
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(NetworkManager.PAIR_RESPONSE)) {
                int pairSuccess = (int)intent.getIntExtra("success", 0);
                if (pairSuccess == 1) {
                    finish();
                } else {
                    pairButton.setEnabled(true);
                    Toast.makeText(getApplicationContext(), "Pairing failed, please try again", Toast.LENGTH_LONG).show();
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.messageReceiver);
        super.onDestroy();
    }
}
