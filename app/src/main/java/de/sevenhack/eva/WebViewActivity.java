package de.sevenhack.eva;

import android.app.ActionBar;
import android.app.Activity;
import android.app.NotificationManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Button;

/**
 * Created by madhavajay on 20/03/2016.
 */

public class WebViewActivity extends Activity {

    private static final String TAG = "WebViewActivity";
    private WebView myWebView;
    public static boolean active = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        active = true;
        ActionBar actionBar = getActionBar();
        actionBar.hide();

        setContentView(R.layout.activity_webview);

        String url = (String) getIntent().getStringExtra("url");
        Integer notificationId = (Integer) getIntent().getIntExtra("notificationId", 1);

        Log.v(TAG, "url extra in web view " + url);

        myWebView = (WebView) findViewById(R.id.web_view);
        url = DataManager.SERVER_URL + url;
        Log.v(TAG, "full url " + url);
        myWebView.loadUrl(url);
    }

    @Override
    protected void onDestroy() {
        Log.v(TAG, "destroy webview");
        if(myWebView != null) {
            myWebView.clearHistory();
            myWebView.clearCache(true);
            myWebView.loadUrl("about:blank");
            myWebView.pauseTimers();
            myWebView = null;
        }
        active = false;
        super.onDestroy();
    }
}
