package de.sevenhack.eva;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ag.sportradar.android.sdk.SRConfiguration;
import ag.sportradar.android.sdk.SRMatchList;
import ag.sportradar.android.sdk.SRSDK;
import ag.sportradar.android.sdk.interfaces.ISRMatchListCallback;
import ag.sportradar.android.sdk.interfaces.ISRSDKCallback;
import ag.sportradar.android.sdk.models.SRException;
import ag.sportradar.android.sdk.models.SRMatch;
import ag.sportradar.android.sdk.models.SRTeamBase;
import ag.sportradar.android.sdk.models.SRTournament;

/**
 * Created by madhavajay on 19/03/2016.
 */
public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";
    private DataManager dataManager;
    private Button unpairButton;
    private Button watchButton;
    private boolean devicePaired = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getActionBar();
        actionBar.hide();

        dataManager = new DataManager(this);

        setContentView(R.layout.activity_main);
        if (dataManager.hasPair()) {
            if (!devicePaired) {
                setupApp();
            }
        }
    }

    private void setupApp() {
        setupButtons();
        setupAlarm();
        setupCallMonitor();
        setupSports();
    }

    private void setupAlarm() {
        int currentNextAlarm = dataManager.getNextAlarm();
        if (currentNextAlarm == -1) {
            createAlarm();
        }
    }

    private void createAlarm() {
        AlarmReceiver.createAlarm(this, dataManager.getAlarmInterval());
    }

    private void setupSports() {
        final Context localContext = this;
        SRSDK.start(getApplicationContext(), getSRSDKConfiguration(), new ISRSDKCallback() {

            @Override
            public void initializationFinished(boolean b, SRException e) {
                Log.v(TAG, "SPORT got sports: " + b);
                SRMatchList matchList = new SRMatchList(new ISRMatchListCallback() {

                    @Override
                    public void fullUpdateReceived(List<SRMatch> list, SRException e) {
                        Log.v(TAG, "SPORT got full update: " +  list.toString());
                        //
                        NetworkManager.getInstance().sendSports(localContext, list);
                    }

                    @Override
                    public void partialUpdateReceived(List<SRMatch> list, SRException e) {

                    }
                }, null, 0, 0);

            }
        });
    }

    public SRConfiguration getSRSDKConfiguration() {
        SRConfiguration config = new SRConfiguration()
                .setAppKey("bad37db073ff689abbd68f587389ce2b")
                .setGcmSender("876143868069")
                .setTimezone(Calendar.getInstance().getTimeZone())
                .setLanguageCode(Locale.getDefault().getLanguage());
        return config;
    }


    private void setupButtons() {
        final Context localContext = this;
        watchButton = (Button) findViewById(R.id.button_watch);
        watchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i;
                i = new Intent(MainActivity.this, WatchActivity.class);
                startActivity(i);
            }
        });

        unpairButton = (Button) findViewById(R.id.button_unpair);
        unpairButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //unpairButton.setEnabled(false);
                dataManager.unPairDevice();
                devicePaired = false;
                finish();
                startActivity(getIntent());
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (!dataManager.hasPair()) {
            showPair();
        } else {
            if (!devicePaired) {
                setupApp();
            }
        }
    }

    private void showPair() {
        Intent i;
        i = new Intent(MainActivity.this, PairActivity.class);
        startActivity(i);
    }

    private void answerCall() {
        final Context localContext = this;

        Log.v(TAG, "trying to answer by showing hidden activity");

        Log.v(TAG, "grab focus once");
        Intent intent = new Intent(localContext, AcceptCallActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        localContext.startActivity(intent);

        try {
            // works when open app
            new Thread(new Runnable() {

                @Override
                public void run() {
                    Log.v(TAG, "answering call");
                    try {
                        Runtime.getRuntime().exec("input keyevent " +
                                Integer.toString(KeyEvent.KEYCODE_HEADSETHOOK));
                    } catch (IOException e) {
                        // Runtime.exec(String) had an I/O problem, try to fall back
                        String enforcedPerm = "android.permission.CALL_PRIVILEGED";
                        Intent btnDown = new Intent(Intent.ACTION_MEDIA_BUTTON).putExtra(
                                Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN,
                                        KeyEvent.KEYCODE_HEADSETHOOK));
                        Intent btnUp = new Intent(Intent.ACTION_MEDIA_BUTTON).putExtra(
                                Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP,
                                        KeyEvent.KEYCODE_HEADSETHOOK));

                        localContext.sendOrderedBroadcast(btnDown, enforcedPerm);
                        localContext.sendOrderedBroadcast(btnUp, enforcedPerm);
                    }


                }

            }).start();

        } catch (Exception e) {
            e.printStackTrace();
        }

                    /*
                    telephonyService.silenceRinger();
                    telephonyService.endCall();
                     */
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(DataManager.CALL_ANSWER)) {
                Log.v(TAG, "got answer call");
                answerCall();
            } else if (intent.getAction().equals(DataManager.CALL_SILENCE)) {
                Log.v(TAG, "got silence call");
            } else if (intent.getAction().equals(DataManager.CALL_HANGUP)) {
                Log.v(TAG, "hang up");

                TelephonyManager tm = (TelephonyManager) context .getSystemService(Context.TELEPHONY_SERVICE);
                Class c = null;
                try {
                    Log.v(TAG, "trying to hang up");
                    c = Class.forName(tm.getClass().getName());
                    Method m = c.getDeclaredMethod("getITelephony");
                    m.setAccessible(true);
                    ITelephony telephonyService=(ITelephony) m.invoke(tm);
                    telephonyService.endCall();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        }
    };

    private void setupCallMonitor() {
        final TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        final Context localContext = this.getApplicationContext();

        //LocalBroadcastManager.getInstance(this).registerReceiver(this.messageReceiver, new IntentFilter(DataManager.CALL_ANSWER));
        //LocalBroadcastManager.getInstance(this).registerReceiver(this.messageReceiver, new IntentFilter(DataManager.CALL_SILENCE));
        //LocalBroadcastManager.getInstance(this).registerReceiver(this.messageReceiver, new IntentFilter(DataManager.CALL_HANGUP));

        // register PhoneStateListener
        PhoneStateListener callStateListener = new PhoneStateListener() {
            public void onCallStateChanged(int state, String incomingNumber)
            {
                //  React to incoming call.
                String number = incomingNumber;
                Log.v(TAG, "incoming number: "+ incomingNumber);
                // If phone ringing
                if(state==TelephonyManager.CALL_STATE_RINGING)
                {
                    final Handler mHandler = new Handler();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.v(TAG, "grab focus once");
                            Intent intent = new Intent(localContext, AcceptCallActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            localContext.startActivity(intent);

                            answerCall();

                        }
                    }, 2000);
                }
                // If incoming call received
                if(state==TelephonyManager.CALL_STATE_OFFHOOK)
                {
                    Log.v(TAG, "call state offhook");
                    //Toast.makeText(getApplicationContext(),"Phone is Currently in A call", Toast.LENGTH_LONG).show();

                    final Handler mHandler = new Handler();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AudioManager audioManager = (AudioManager) localContext.getSystemService(Context.AUDIO_SERVICE);
                            audioManager.setMode(AudioManager.MODE_NORMAL);
                            audioManager.setSpeakerphoneOn(true);
                            Log.v(TAG, "turning on speaker phone");

                        }
                    }, 500);
                }


                if(state==TelephonyManager.CALL_STATE_IDLE)
                {
                    Log.v(TAG, "call state idle");
                    //Toast.makeText(getApplicationContext(),"phone is neither ringing nor in a call", Toast.LENGTH_LONG).show();
                }

                String contactName = "";
                if (number != null && !number.equals("")) {
                    contactName = getContactName(getApplicationContext(), number);

                    //String kalkiNumber = "01607136935";
                    QuickContactHelper quickContactHelper = new QuickContactHelper(getApplicationContext(), number);

                    Bitmap contactBitmap = quickContactHelper.getThumbnail();

                    String thumbnailBase64 = "";
                    if (contactBitmap != null) {
                        thumbnailBase64 = encodeToBase64(contactBitmap, Bitmap.CompressFormat.JPEG, 70);
                    }

                    Log.v(TAG, "contact name " + contactName);
                    Log.v(TAG, "contact number " + number);
                    Log.v(TAG, "thumbnail " + thumbnailBase64);
                    NetworkManager.getInstance().pushCallNotification(localContext, state, contactName, number, thumbnailBase64);
                } else {
                    NetworkManager.getInstance().pushCallNotification(localContext, state, null, null, null);
                }

            }
        };
        telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);

    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME
        }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }


}
