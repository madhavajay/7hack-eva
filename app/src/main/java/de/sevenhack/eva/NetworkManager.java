package de.sevenhack.eva;

/**
 * Created by madhavajay on 19/03/2016.
 */

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ag.sportradar.android.sdk.models.SRMatch;
import ag.sportradar.android.sdk.models.SRTeamBase;
import ag.sportradar.android.sdk.models.SRTournament;

public class NetworkManager {

    private final String TAG = "NetworkManager";
    public static final String PAIR_RESPONSE = "pair_response";
    public static final String SPORTRADAR_RESPONSE = "sportsradar";
    public static final String POLL_RESPONSE = "poll_response";
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private boolean isPolling = false;

    private DataManager dataManager;

    private Context context;
    private Application application;

    private final Gson gson = new Gson();

    private static NetworkManager ourInstance = new NetworkManager();

    public static NetworkManager getInstance() {
        return ourInstance;
    }

    public NetworkManager() {

    }

    private String getEndpointUrl(DataManager dataManager, String endpoint) {
        return dataManager.getServerHost() + endpoint;
    }

    public void sendSports(Context context, List<SRMatch> list) {
        this.context = context;
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        int i = 0;

        //date = (Date)((DateFormat) formatter).parse("2011-04-13 05:00");
        SimpleDateFormat formatter = new SimpleDateFormat("k:mm");

        for(SRMatch match : list) {
            if (i > 10) {
                continue;
            }
            SRTeamBase team1 = match.getTeam1();
            SRTeamBase team2 = match.getTeam2();
            SRTournament tournament = match.getTournament();
            Date date = match.getMatchDate();

            String team1name = team1.getName();
            String team2name = team2.getName();
            String tournamentName = tournament.getName();
            String dateString = formatter.format(date);
            String team1Image = team1.teamCrestURL(true);
            String team2Image = team2.teamCrestURL(true);

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("team_name_1", team1name);
            map.put("team_name_2", team2name);
            map.put("tournament_name", tournamentName);
            map.put("date", dateString);
            map.put("team_image_1", team1Image);
            map.put("team_image_2", team2Image);

            data.add(map);
            Log.v(TAG, "adding sports item to map");
            i++;
        }

        Gson gson = new Gson(); // Or use new GsonBuilder().create();
        final String json = gson.toJson(data); // serializes target to Json

        Log.v(TAG, "sports json sending "+ json);


        new Thread(new Runnable() {

            @Override
            public void run() {
                makeSendSports(json);
            }
        }).start();

    }

    public void makeSendSports(String json) {
        dataManager = new DataManager(this.context);
        String url = getEndpointUrl(dataManager, "sportradar");
        Log.v(TAG, "url: " + url);
        String clientId = dataManager.getClientId();

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .put(RequestBody.create(JSON, json))
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();

        } catch (Exception e) {
            e.printStackTrace();

        }

        if (response != null) {
            Log.v(TAG, "poll res: " + response.toString());
        } else {
            Log.v(TAG, "retry sports");
            this.postNotification(SPORTRADAR_RESPONSE, "retry", "");
        }

    }

    public void pushCallNotification(Context context, final int callState, final String contactName, final String phoneNumber, final String thumbnailBase64) {
        this.context = context;
        new Thread(new Runnable() {

            @Override
            public void run() {
                makePushCallNotification(callState, contactName, phoneNumber, thumbnailBase64);
            }
        }).start();
    }

    public void makePushCallNotification(int callState, String contactName, String phoneNumber, String thumbnailBase64) {
        dataManager = new DataManager(this.context);
        String url = getEndpointUrl(dataManager, "event");
        String clientId = dataManager.getClientId();

        String jsonString = "{}";

        if (contactName != null && phoneNumber != null && thumbnailBase64 != null) {
            String base64UrlEncoded = "";

            try {
                //url += "?messageType=pushCall&clientId=" + URLEncoder.encode(clientId, "UTF-8").toString();
                base64UrlEncoded = URLEncoder.encode(thumbnailBase64, "UTF-8").toString();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            jsonString = "{\"event\":{\"devices\":[\"tv\"], \"type\":\"phone\", \"data\":{\"callState\":\"" + callState +"\",\"contactName\":\"" + contactName + "\"," +
                    "\"contactNumber\":\"" + phoneNumber + "\",\"contactImage\":\"" + base64UrlEncoded + "\"}}}";
            Log.v(TAG, "json for call contact " + jsonString);
        } else {
            jsonString = "{\"event\":{\"devices\":[\"tv\"], \"type\":\"phone\", \"data\":{\"callState\":\"" + callState +"\"}}}";
            Log.v(TAG, "json for call no contact" + jsonString);
        }

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .put(RequestBody.create(JSON, jsonString))
                .build();

        Log.v(TAG, "send data to server " + jsonString);

        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (response != null) {
            Log.v(TAG, "poll res: " + response.toString());
        }

        if (response != null && response.code() >= 200 && response.code() <= 299) {
            String responseJson = null;
            try {
                responseJson = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.v(TAG, "pair responseJson: " + responseJson);
            this.postNotification(POLL_RESPONSE, "pollData", responseJson);
        } else {
            this.postNotification(POLL_RESPONSE, "pollData", "");
        }
    }

    public void pollServer(Context context) {
        if (isPolling) {
            Log.v(TAG, "still polling");
            return;
        } else {
            Log.v(TAG, "not still polling");
        }
        isPolling = true;
        this.context = context;
        new Thread(new Runnable() {

            @Override
            public void run() {
                makePollServer();
            }
        }).start();
    }

    public String makeJsonPayload(Map<String, String> payload) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(payload);
        return json;
    }

    public void makePollServer() {
        dataManager = new DataManager(this.context);
        String url = getEndpointUrl(dataManager, "state");
        Log.v(TAG, "url: " + url);
        String clientId = dataManager.getClientId();

        HashMap<String, String> payload = new HashMap<String, String>();
        payload.put("deviceType", "phone");
        payload.put("clientId", clientId);

        String jsonString = makeJsonPayload(payload);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .put(RequestBody.create(JSON, jsonString))
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            isPolling = false;
        } catch (Exception e) {
            e.printStackTrace();
            isPolling = false;
        }

        if (response != null) {
            Log.v(TAG, "poll res: " + response.toString());
        }

        if (response != null && response.code() >= 200 && response.code() <= 299) {
            String responseJson = null;
            try {
                responseJson = response.body().string();
                dataManager.setStateJson(responseJson);

            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.v(TAG, "poll responseJson: " + responseJson);
            this.postNotification(POLL_RESPONSE, "pollData", responseJson);
        } else {
            Log.v(TAG, "post empty poll response notification");
            this.postNotification(POLL_RESPONSE, "pollData", "");
        }
    }

    public void pairDevice(Application application, final String pairCode, final String githubUsername) {
        this.application = application;
        this.context = this.application.getApplicationContext();
        new Thread(new Runnable() {

            @Override
            public void run() {
                makePairDevice(pairCode, githubUsername);
            }
        }).start();
    }

    public void makePairDevice(String pairCode, String githubUsername) {
        dataManager = new DataManager(this.context);
        String url = getEndpointUrl(dataManager, "pair");
        Log.v(TAG, "url: " + url);

        String userId = dataManager.getAdvertisingId();

        HashMap<String, String> payload = new HashMap<String, String>();
        payload.put("deviceType", "phone");
        payload.put("pairCode", pairCode);
        payload.put("github", githubUsername);
        payload.put("uuid", userId);

        String jsonString = makeJsonPayload(payload);

        Log.v(TAG, jsonString);

        Log.v(TAG, "sending response");
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .put(RequestBody.create(JSON, jsonString))
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            Log.v(TAG, "got response " +response);;
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.v(TAG, "Login response: " + response);
        if (response != null) {
            Log.v(TAG, "Login res: " + response.toString());
        }

        if (response != null && response.code() >= 200 && response.code() <= 299) {
            String responseJson = null;
            try {
                responseJson = response.body().string();

                dataManager.setClientJson(responseJson);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.v(TAG, "pair responseJson: " + responseJson);
            this.postNotification(PAIR_RESPONSE, "success", 1);
        } else {
            Log.v(TAG, "no response");
            this.postNotification(PAIR_RESPONSE, "success", 0);
        }
    }

    // LocalBroadcastManager Helper Methods
    private void postNotification(String intentName, String name, int value) {
        Intent intent = new Intent(intentName);
        intent.putExtra(name, value);
        LocalBroadcastManager.getInstance(this.context).sendBroadcast(intent);
    }

    private void postNotification(String intentName, String name, LinkedHashMap<String, String> value) {
        Intent intent = new Intent(intentName);

        JSONObject jsonObject = new JSONObject(value);
        String jsonValue = jsonObject.toString();

        intent.putExtra(name, jsonValue);
        LocalBroadcastManager.getInstance(this.context).sendBroadcast(intent);
    }

    private void postNotification(String intentName, String name, String value) {
        Intent intent = new Intent(intentName);

        intent.putExtra(name, value);
        LocalBroadcastManager.getInstance(this.context).sendBroadcast(intent);
    }
}
