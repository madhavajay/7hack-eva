package com.android.internal.telephony;

/**
 * Created by madhavajay on 20/03/2016.
 */

public interface ITelephony {

    boolean endCall();

    void answerRingingCall();

    void silenceRinger();

}